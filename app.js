var itemList = document.querySelector('#items');
var form = document.querySelector('#addForm');
var filter = document.querySelector('#filter');

//event Listeners Area
form.addEventListener('submit', addnewItem); //add items
itemList.addEventListener('click', removeItem);//remove items
filter.addEventListener('keyup', filterItem); //filter items


//add items
function addnewItem(e) {
    e.preventDefault();

    var input = document.querySelector('#item').value;
    var text = document.createTextNode(input);
    var newItem = document.createElement('li');
    var button = document.createElement('button');
    button.appendChild(document.createTextNode('X'));
    button.className = 'btn btn-danger delete';
    newItem.appendChild(text);
    newItem.appendChild(button);
    items.appendChild(newItem);
};

//remove items
function removeItem(e) {
    if (e.target.classList.contains('delete')) {
        if (confirm("are you sure")) {
            var li = e.target.parentElement
            items.removeChild(li);
        }
    }
}

//filter items
function filterItem(e) {
    var text = e.target.value.toLowerCase();
    var items = itemList.getElementsByTagName('li');
    Array.from(items).forEach((item) => {
        if(item.firstChild.textContent.toLowerCase().indexOf(text) != -1){
            item.style.display = 'block';
        }else{
            item.style.display = 'none';
        }
    })
}

